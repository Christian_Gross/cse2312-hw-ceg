.global main
.func main
   
main:
    BL  _prompt1             @ branch to prompt procedure with return (grabs first number)
    BL  _scanf              @ branch to scanf procedure with return
    MOV R1, R0              @ move return value R0 to argument register R1

    BL  _prompt2             @ branch to prompt procedure with return (grabs operation code)
    BL  _scanf              @ branch to scanf procedure with return
    MOV R3, R0              @ move return value R0 to argument register R1

    BL  _prompt1             @ branch to prompt procedure with return (grabs second number)
    BL  _scanf              @ branch to scanf procedure with return
    MOV R2, R0              @ move return value R0 to argument register R1

    BL  _eval		     @ evaluate opcode
         		    
    BL  _printf             @ branch to print procedure with return
    B   _exit               @ branch to exit procedure with no return
   
_exit:  
    MOV R7, #4              @ write syscall, 4
    MOV R0, #1              @ output stream to monitor, 1
    MOV R2, #21             @ print string length
    LDR R1, =exit_str       @ string at label exit_str:
    SWI 0                   @ execute syscall
    MOV R7, #1              @ terminate syscall, 1
    SWI 0                   @ execute syscall


_prompt1:
    MOV R7, #4              @ write syscall, 4
    MOV R0, #1              @ output stream to monitor, 1
    MOV R2, #31             @ print string length
    LDR R1, =prompt1_str     @ string at label prompt1_str:
    SWI 0                   @ execute syscall
    MOV PC, LR              @ return


_prompt2:
    MOV R7, #4              @ write syscall, 4
    MOV R0, #1              @ output stream to monitor, 1
    MOV R2, #31             @ print string length
    LDR R1, =prompt2_str     @ string at label prompt2_str:
    SWI 0                   @ execute syscall
    MOV PC, LR              @ return
       

_printf:
    MOV R4, LR              @ store LR since printf call overwrites
    LDR R0, =printf_str     @ R0 contains formatted string address
    MOV R1, R1              @ R1 contains printf argument (redundant line)
    BL printf               @ call printf
    B _exit

    @MOV PC, R4              @ return
    

_scanf:
    PUSH {LR}                @ store LR since scanf call overwrites
    SUB SP, SP, #4          @ make room on stack
    LDR R0, =format_str     @ R0 contains address of format string
    MOV R1, SP              @ move SP to R1 to store entry on stack
    BL scanf                @ call scanf
    LDR R0, [SP]            @ load value at SP into R0
    ADD SP, SP, #4          @ restore the stack pointer
    POP {PC}                 @ return


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

_eval:
    CMP R3, #'+'            @ compare against the constant char '@'
    BEQ _add	            @ branch to equal handler

    CMP R3, #'-'            @ compare against the constant char '@'
    BEQ _sub	            @ branch to equal handler

    CMP R3, #'*'            @ compare against the constant char '@'
    BEQ _mult	            @ branch to equal handler

    CMP R3, #'m'            @ compare against the constant char '@'
    BEQ _max	            @ branch to equal handler
    
    MOV PC, LR


_add:
    ADD R1, R1, R2
    B _printf

_sub:
    SUB R1, R1, R2
    B _printf


_mult:
    MUL R1, R1, R2
    B _printf


_max:
    





@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
.data
format_str:     .asciz      "%d"
prompt1_str:     .asciz      "Type a number and press enter: "
prompt2_str:     .ascii      "Type an operation and press enter: "
printf_str:     .asciz      "The answer is: %d\n"
exit_str:       .ascii      "Terminating program.\n"